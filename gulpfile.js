
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    cleanCSS = require('gulp-clean-css'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync').create(),
    notifier = require('node-notifier'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    imageresize = require('gulp-image-resize'),
    shell = require('gulp-shell'),
    yargs = require('yargs').argv;


var paths = require("./gulp_settings/paths.json");

var preWatch = async function () {
    // await gulp.src(paths.styles.libraries)
    //     .pipe(concat("libraries.css"))
    //     .pipe(cleanCSS())
    //     .pipe(gulp.dest(paths.styles.dest))
    //
    // await gulp.src(paths.scripts.libraries)
    //     .pipe(concat("libraries.js"))
    //     .pipe(gulp.dest(paths.scripts.dest.folder));


    browserSync.init({
        proxy: paths.proxy,
        injectChanges: true,
        open: false
    }, () => browserSync.reload());

};

var afterWatch = async function () {
    notifier.notify({
        title: "Now you can develop",
        message: "Develop",
    });
};

var watch = async function () {
    const stylesWatcher = gulp.watch(paths.styles.all, styles);
    const scriptsWatcher = gulp.watch(paths.scripts.all, scripts);
    const imageWatcher = gulp.watch(paths.images.all, images);

    //Waiting for the change in PHP and HTML files, then reload browser
    const htmlWatcher = gulp.watch(paths.html, (done) => {
        browserSync.reload();
        done(); //ensure, that this task repeat, without the done() function it runs only once
    });
};


/**
 * Gulp task for styles in development,
 * which includes Sourcemaps, LESS compilation and browser injections of changes
 */
var styles = async function () {
    await gulp.src(paths.styles.src)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', err => {
            console.log(err);
            notifier.notify({
                title: err.file.substring(err.file.lastIndexOf('/') + 1) + "::" + err.line,
                message: err.message,
            });
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(browserSync.stream());
};


/**
 * Gulp task for distribution of the styles.
 * Compiles less files, autoprefix css and minifi it
 */
var stylesDist = async function () {
    return gulp.src(paths.styles.src)
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(cleanCSS())
        .pipe(gulp.dest(paths.styles.dest));
};


/**
 * Gulp task for developing scripts.
 * Concat all files listed in source file into one file and puts it into distribution folder
 */
var scripts = async function () {
    await gulp.src(require(paths.scripts.src))
        .pipe(concat(paths.scripts.dest.file))
        .pipe(gulp.dest(paths.scripts.dest.folder))
        .pipe(browserSync.stream());
};


/**
 * Gulp task for distribution of scripts
 * Involves task for developing by adding uglify plugin for minification
 * and creates .min version of the scripts
 * @returns {Promise<void>}
 */
var scriptsDist = async function () {
    await gulp.src(require(paths.scripts.src))
        .pipe(concat(paths.scripts.dest.minifiedFile))
        .pipe(uglify()).on("error", function (err) {
            console.log(err.cause);
            notifier.notify({
                title: "JS uglify: " + err.cause.filename + "::" + err.cause.line,
                message: err.cause.message,
            });
        })
        .pipe(gulp.dest(paths.scripts.dest.folder))
        .pipe(browserSync.stream());
};


//TODO Create task for svg minification, that conpiles SVG in the same folder to allow editation

/**
 * Gulp task for minification of images
 * Takes all images from source folder and compiles it into distribution folder
 * Reads JPEG, PNG and SVGs
 */
var images = async function () {
    return gulp.src(paths.images.all)
        .pipe(imagemin([
            imagemin.jpegtran({
                progressive: true,
            }),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest(paths.images.dest));
};


/**
 * Gulp task for minification of images
 * Takes all images from source folder and compiles it into distribution folder
 * Reads JPEG, PNG and SVGs
 */
var resizeImages = async function () {
    var resizeOptions = {};
    if(yargs.w) resizeOptions.width = yargs.w;
    if(yargs.h) resizeOptions.height = yargs.h;
    if(yargs.crop) resizeOptions.crop = validateBool(yargs.crop);

    return gulp.src(paths.images.all)
        .pipe(imageresize(resizeOptions))
        .pipe(imagemin([
            imagemin.jpegtran({
                progressive: true,
            }),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest(paths.images.dest));
};

function validateBool(input) {
    var trues = [1, "1", "true", true];
    if(trues.indexOf(input) != -1) return true;
    else return false;
}


/**
 * Stores project on git and sends it to server via FTP
 * Takes one parameter, message for commit message
 */
var git = async function () {
    var message = yargs.m ? yargs.m : "";
    if(!yargs.m){
        console.log("Sorry, but you must add commit message. You can add it with argument --m='Your message'");
    } else {
        return gulp.src("index.html")
            .pipe(shell([
                "git add .",
                "git commit -m '" + message + "'",
                "git pull",
                "git push",
                "git ftp push"
            ]));
    }
};


gulp.task('styles', styles);
gulp.task('scripts', scripts);
gulp.task('images', images);
gulp.task('resize-images', resizeImages);

gulp.task('dev', gulp.series(styles, scripts, preWatch, watch, afterWatch));

gulp.task('dist', gulp.series(stylesDist, scriptsDist, git));

