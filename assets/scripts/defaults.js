/*
    Author: Tomas Gerzicak
    Version: 1.0
    Description:
*/

/**
 * Animate every link that goes to some anchor
 */
$(function() {
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) { $('html,body').animate({
                scrollTop: target.offset().top - 100 }, 700);
                return false;
            }
        }
    });
});



// ************************************
// ****** Small jQuery functions ******
// ************************************

/**
 *
 * @param selector Query selector
 * @returns {boolean} if element exists
 */
function elementExist(selector){
    return $(selector).length > 0;
}

/**
 * Checks if element has children
 * @returns {boolean}
 */
function hasChildren() {
    return this.children().length > 0;
}

/**
 *
 * @param selector Query selector on element to be scrolled to
 * @param {number} offset (optional) Offset from the element. Default value is 100
 * @param {number} duration (optional) determines, how long will animation lasts
 */
function smoothScrollTo(selector, offset, duration)
{
    var target = $(selector);
    $("body, html").animate({
        scrollTop: target.offset().top - offset
    }, duration);
}


function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
