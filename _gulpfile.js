
var gulp = require('gulp'),
    less = require('gulp-less'),
    cleanCSS = require('gulp-clean-css'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync').create(),
    notifier = require('node-notifier'),
    imagemin = require('gulp-imagemin'),
    concat = require('gulp-concat')

var paths = require("./gulp_settings/paths.json");

var watch = async function () {
    const stylesWatcher = gulp.watch(paths.styles.all, styles);
    const scriptsWatcher = gulp.watch(paths.scripts.all, scripts);
    const htmlWatcher = gulp.watch("*.php", (done) => {
        browserSync.reload();
        done(); //ensure, that this task repeat, without the done() function it runs only once
    });
    const imageWatcher = gulp.watch(paths.images, images);
    const iconWatcher = gulp.watch(paths.icons, icons);
    // const scriptsWatcher = gulp.watch(paths.scripts.all, styles);
};

var preDev = async function () {
    browserSync.init({
        proxy: paths.proxy,
        injectChanges: true,
        open: false
    }, () => browserSync.reload());

}

var styles = async function () {
    return gulp.src(paths.styles.src)
        .pipe(sourcemaps.init())
        .pipe(less().on('error', err => {
            notifier.notify({
                title: err.filename.substring(err.filename.lastIndexOf('/') + 1) + "::" + err.line,
                message: err.message,
            });
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(browserSync.stream());
};

var stylesDist = async function () {
    return gulp.src(paths.styles.src)
        .pipe(less())
        .pipe(autoprefixer())
        .pipe(cleanCSS)
        .pipe(gulp.dest(paths.styles.dest));

};

var scripts = async function () {
    await gulp.src(require(paths.scripts.src))
        .pipe(concat("scripts.js"))
        .pipe(gulp.dest(paths.scripts.dest))
        .pipe(browserSync.stream());
};

var images = async function () {
    return gulp.src("assets/images/**/*")
        .pipe(imagemin([
            imagemin.jpegtran({
                progressive: true,
                // arithmetic: true
            }),
            imagemin.optipng({optimizationLevel: 5}),
        ]))
        .pipe(gulp.dest("dist/images/"));
};


var icons = async function () {
    return gulp.src("assets/icons/*")
        .pipe(imagemin([
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest("dist/icons"))
};


var htmlFiles = async function () {
    await gulp.src("**/*.php")
        .pipe(browserSync.stream());
}
gulp.task('styles', styles);
gulp.task('scripts', scripts);
gulp.task('images', images);
gulp.task('icons', icons);
gulp.task('dev', gulp.series(preDev, images, icons, styles, scripts,  watch));
gulp.task('dist-styles', stylesDist);
gulp.task('dist', gulp.parallel(stylesDist, ))