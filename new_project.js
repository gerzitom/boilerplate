/*
    Author: Tomas Gerzicak
    Version: 1.0
    Description:
*/

var Handlebars = require('handlebars');
var fs = require('fs');
var readline = require('readline');
var chalk = require('chalk');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('Name of the project: ', (name) => {
    rl.question('Description: ', description => {
        rl.question('Git repository: ', repository => {
            fs.readFile('./src/templates/package.json','utf8', function (err, file) {
                var template = Handlebars.compile(file);
                var data = {
                    name: name,
                    description: description,
                    repository: repository
                };
                fs.writeFile('package.json', template(data), '', err => {
                    if(err == null)
                        console.log('You have finally created a new CREEPY project!' + chalk.bgGreen.black('CONGRATULATIONS'));
                    else
                        console.log(err);
                });
                rl.close();
            })
        });
    });
});

