<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="crp">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <?php // Google Chrome Frame for IE ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php is_front_page() ? bloginfo('name') : wp_title(''); ?></title>


    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <?php wp_head(); ?>

    <link rel="apple-touch-icon" sizes="180x180" href="<?= get_template_directory_uri() ?>/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= get_template_directory_uri() ?>/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= get_template_directory_uri() ?>/favicon-16x16.png">
    <link rel="manifest" href="<?= get_template_directory_uri() ?>/site.webmanifest">
    <link rel="mask-icon" href="<?= get_template_directory_uri() ?>/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
</head>

<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
?>

<body <?php body_class(); ?>>
<main>

