<?php

define('IMAGES',  get_template_directory_uri().'/assets/images');

add_filter('max_srcset_image_width', function($max_srcset_image_width, $size_array){
    return 2560;
}, 10, 2);


function creepy_scripts_and_styles() {

    // Add custom fonts, used in the main stylesheet.
    wp_enqueue_style( 'creepy-styles', get_template_directory_uri().'/dist/style.css', '', '0.1.29' );

//    wp_enqueue_script( 'swiper', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.0/js/swiper.min.js', false, '', true);

    wp_enqueue_script( 'creepy-script', get_theme_file_uri( '/dist/scripts.js' ), array('jquery'), '0.0.2', true );
}
add_action( 'wp_enqueue_scripts', 'creepy_scripts_and_styles' );



/*********************
CREEPY WP customize
 *********************/

//function my_login_stylesheet() {
//    wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/dist/style-login.css' );
//}
//add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );
//function my_login_logo_url() {
//    return "http://creepy.studio";
//}
//add_filter( 'login_headerurl', 'my_login_logo_url' );
//
//function my_login_logo_url_title() {
//    return 'Powered by Creepy Studio';
//}
//add_filter( 'login_headertitle', 'my_login_logo_url_title' );

function remove_footer_admin () {
    echo 'Made with love by <a href="http://creepy.cz/" target="_blank"  rel="copyright">Creepy studio</a>.';
}
add_filter('admin_footer_text', 'remove_footer_admin'); //change admin footer text
